package fr.afcepf.al34.project1.spring.springboutique.web;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.ManagedBean;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.context.SessionScoped;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;

import fr.afcepf.al34.project1.spring.springboutique.business.AuthenticationManager;
import fr.afcepf.al34.project1.spring.springboutique.business.CartService;
import fr.afcepf.al34.project1.spring.springboutique.business.ClientService;
import fr.afcepf.al34.project1.spring.springboutique.business.CredentialsService;
import fr.afcepf.al34.project1.spring.springboutique.business.ProductService;
import fr.afcepf.al34.project1.spring.springboutique.entity.Batch;
import fr.afcepf.al34.project1.spring.springboutique.entity.Cart;
import fr.afcepf.al34.project1.spring.springboutique.entity.Client;
import fr.afcepf.al34.project1.spring.springboutique.entity.Credentials;
import fr.afcepf.al34.project1.spring.springboutique.entity.Product;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@ManagedBean
@Getter @Setter @NoArgsConstructor
@SessionScoped
public class CatalogueMBean {

	private String login;
	private String clearPassword;
	private Cart cart;
	private List<Product> allProducts;
	private List<Product> productsInCart;
	private Product currentProduct;
	private Client client;
	private int nbArticles;
	private String message;
	
	@Inject
	private CartService cartService;
	
	@Inject
	private ProductService productService;
	
	@Inject
	private CredentialsService credentialsService;
	
	@Inject
	private ClientService clientService;
	
	
	@PostConstruct
	public void init() {
		client = new Client();
		productsInCart = new ArrayList<Product>();
		cart = cartService.saveInBase(new Cart());
		cart.setBatches(new ArrayList<Batch>());
		allProducts = productService.getAllProducts();
		nbArticles = 0;
		System.out.println("init------------");
	}
	public String doConnection() {
		Credentials credentials = credentialsService.findWithLogin(this.login);
		if(credentials != null) {
			try {
				if(AuthenticationManager.authenticate(clearPassword, credentials)) {
					client = clientService.findWithCredentials(credentials);
					cart = client.getCart();
					nbArticles = nbProducts();
					allProducts = productService.getAllProducts();
					return "clientMain.xhtml?faces-redirect=true";
				} else {

					message = "identifiants incorrects";
					return "clientMain.xhtml?faces-redirect=true";
				}

			} catch (Exception e) {
				e.printStackTrace();
				message = e.getMessage();
				return "login.xhtml";
			}
		} else {
			message = "identifiants incorrects";
			return "login.xhtml";
		}
	}
	
	public String doDisconnect() {
		init();
		System.out.println("-------deconnexion" + client.toString());
		return "clientMain.xhtml?faces-redirect=true";
	}
	
	public boolean isInCart(Product p) {
		boolean result = false;
		for (Batch batch : getCart().getBatches()) {
			if(batch.getProduct().getId() == p.getId()) {
				return true;
			}
		}
		return result;
	}
	
	public String doAddToCart(Product p) {
		if (isInCart(p)) {
			for (Batch batch : cart.getBatches()) {
				if(batch.getProduct().getId() == p.getId()) {
					batch.setQuantity(batch.getQuantity()+1);
					cartService.saveInBase(batch);
				}
			}
		} else {
			Batch batch = cartService.saveInBase(new Batch(1, LocalDate.now(), p, cart));
			cart.getBatches().add(batch);
		}
		cart = cartService.saveInBase(cart);
		nbArticles++;
		return "clientMain.xhtml?faces-redirect=true";
	}
	
	public int nbProducts() {
		int result = 0;
		for (Batch batch : cart.getBatches()) {
				result += batch.getQuantity();
		}
		
		return result;
	}
	
	public String doDisplayCart() {
		productsInCart = cartService.findWithCart(cart);
		return "clientCart.xhtml?faces-redirect=true";
	}
	
	public void doRemoveFromCart(Product p) {
		Batch batchToRemove = new Batch();
		for (Batch batch : cart.getBatches()) {
			if(batch.getProduct().getId() == p.getId()) {
					batchToRemove = batch;
			}
		}
		cart.getBatches().remove(batchToRemove);
		cart = cartService.saveInBase(cart);
		cartService.removeFromCart(batchToRemove);
		productsInCart = cartService.findWithCart(cart);
		nbArticles = nbProducts();
	}
	
	public int calculateQuantity(Product p) {
		int result = 0;
		for (Batch batch : cart.getBatches()) {
			if(batch.getProduct().getId() == p.getId()) {
				result = batch.getQuantity();
			}
		}
		return result;
	}
	
	public void doDecreaseCart(Product p) {
		boolean removeP = false;
		for (Batch batch : cart.getBatches()) {
			if(batch.getProduct().getId() == p.getId()) {
				if(batch.getQuantity() > 1) {
					batch.setQuantity(batch.getQuantity()-1);
					cartService.saveInBase(batch);
					cartService.saveInBase(cart);
					nbArticles--;
				} else {
					removeP = true;
				}
			}
		}
		if(removeP) {
			doRemoveFromCart(p);
		}
	}
	
	public void doIncreaseCart(Product p) {
		for (Batch batch : cart.getBatches()) {
			if(batch.getProduct().getId() == p.getId()) {
					batch.setQuantity(batch.getQuantity()+1);
					cartService.saveInBase(batch);
					cartService.saveInBase(cart);
					nbArticles++;
			}
		}
	}
	
	public double calculateAmount(Product p) {
		double result = 0;
		for (Batch batch : cart.getBatches()) {
			if(batch.getProduct().getId() == p.getId()) {
				result = batch.getQuantity() * p.getPrice();
			}
		}
		return result;
	}
	
	public double calculateTotalAmount() {
		double result = 0;
		for (Batch batch : cart.getBatches()) {
			result += batch.getQuantity() * batch.getProduct().getPrice();
		}
		return result;
	}
	
	public String doCartValidation() {
		if(client.getId() != null) {
			return "validateCart.xhtml?faces-redirect=true";
		} else {
			return "createAccount.xhtml?faces-redirect=true";
		}
		
	}
	
	public String doCartPayment() {
		Product productToRemove = new Product();
		for(Product p : allProducts) {
			for (Batch batch : cart.getBatches()) {
				if(batch.getProduct().equals(p)) {
					productToRemove = p;
				}
			}
			doRemoveFromCart(p);
		}
		return "clientMain.xhtml?faces-redirect=true";
	}
	
	
	public String getPromo(Product p) {
		String result = "";
		if(p.getPromotion() != null) {
			result = "-" + p.getPromotion().getAmount() + "%";
		}
		return result;
	}
	
	public String calculateNewPrice(Product p) {
		String result = "";
		if(p.getPromotion() != null) {
			double newPrice = p.getPrice()*(100-p.getPromotion().getAmount())/100;
			result = String.valueOf((int) newPrice) + " €";
		}
		return result;
	}
	
	public String doGetProductDetails(Product p) {
		currentProduct = p;
		currentProduct.setPhotos(p.getPhotos());
		return "productDetails.xhtml?faces-redirect=true";
	}
	
	
	
	
	
	
}
