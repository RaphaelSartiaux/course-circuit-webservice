package fr.afcepf.al34.project1.spring.springboutique.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter @NoArgsConstructor
@Entity
@Table(name = "inventory")
public class Inventory implements Serializable{
    
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

    private int quantity;

    @Column(name = "min_stock")
    private int minStock;

    @ManyToOne
    @JoinColumn(referencedColumnName = "id")
    private Type type;

	public Inventory(int quantity, int minStock, Type type) {
		
		this.setQuantity(quantity);
		this.setMinStock(minStock);
		this.setType(type);
	}
    
    
    
}
