package fr.afcepf.al34.project1.spring.springboutique.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter @NoArgsConstructor
@Entity
@Table(name = "composition")
public class Composition implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String name;

    private int quantity;

    @Column(name = "is_natural")
    private boolean isNatural;

	public Composition(String name, boolean isNatural) {
		super();
		this.name = name;
		this.isNatural = isNatural;
	}

	@OneToMany(mappedBy = "composition")
    private List<Product> product = new ArrayList<Product> ();

}
