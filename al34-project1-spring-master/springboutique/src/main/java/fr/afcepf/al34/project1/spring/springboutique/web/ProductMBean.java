package fr.afcepf.al34.project1.spring.springboutique.web;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.ManagedBean;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import fr.afcepf.al34.project1.spring.springboutique.business.CategoryService;
import fr.afcepf.al34.project1.spring.springboutique.business.CustomerGenderService;
import fr.afcepf.al34.project1.spring.springboutique.business.InventoryService;
import fr.afcepf.al34.project1.spring.springboutique.business.ProductService;
import fr.afcepf.al34.project1.spring.springboutique.business.SportProductService;
import fr.afcepf.al34.project1.spring.springboutique.entity.Category;
import fr.afcepf.al34.project1.spring.springboutique.entity.Composition;
import fr.afcepf.al34.project1.spring.springboutique.entity.CustomerGender;
import fr.afcepf.al34.project1.spring.springboutique.entity.Inventory;
import fr.afcepf.al34.project1.spring.springboutique.entity.Photo;
import fr.afcepf.al34.project1.spring.springboutique.entity.Product;
import fr.afcepf.al34.project1.spring.springboutique.entity.Sport;
import fr.afcepf.al34.project1.spring.springboutique.entity.SportProduct;
import fr.afcepf.al34.project1.spring.springboutique.entity.Type;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@ManagedBean
@RequestScoped
@Getter @Setter @NoArgsConstructor
public class ProductMBean {

	private String technicalInfo="";
	private String[] technicalInfos;
	private int[] listOfSports;
	
	private List<Type> types;
	private List<Product> products;
	private List<String> colors; 
	private List<String> sizes;
	private List<Sport> sports;
	private List<Sport> myListOfSports;
	private List<Category> categories;
	private List<Composition> compositions;
	private List<CustomerGender> genders;
	private List<CustomerGender> customerGenders;
	private List<Inventory> inventories;
	private List<SportProduct> sportProducts;
	private List<Photo> photos;
	private String name;
	private String description;
	private Double price;
	private String color;
	private String size;
	private Double cost;
	private Double vat;
	private int stock;
	private int minStock;
	private int gender;
	private int composition;
	private Double compositionQuantity;
	private int sport;
	private int category;
	private String message;
	private String fileName;

	@Inject
	ProductService productService;
	@Inject
	InventoryService inventoryService;
	@Inject
	CategoryService categoryService;
	@Inject
	CustomerGenderService customerGenderService;
	@Inject
	SportProductService sportProductService;
	
	public String[] getTechnicalInfosValue() {

		technicalInfos = new String[6];
		technicalInfos[0] = "Résistance à l'abrasion";
		technicalInfos[1] = "Imperméable";
		technicalInfos[2] = "Elastique";
		technicalInfos[3] = "Anti-transpiration";
		technicalInfos[4] = "Adhérent";
		technicalInfos[5] = "Accroche";

		//....

		return technicalInfos;
	}

	public String getTechnicalInfosInString() {
		for(String e : this.technicalInfos) {
			this.technicalInfo+= e + " , ";
		}
		return this.technicalInfo;
	}

	@PostConstruct
	public void init() {
		this.types=this.inventoryService.getAllTypes();
		this.colors= inventoryService.getAllColors();
		this.sizes= inventoryService.getAllSizes();
		this.sports= categoryService.getAllSports();
		this.categories= categoryService.getAllCategories();
		this.compositions= categoryService.getAllCompositions();
		this.genders=customerGenderService.getAllCustomerGenders();
		this.products = productService.getAllProducts();
	}

	public String doDisplayProduct() {
		return "productDetails.xhtml?faces-redirect=true";
	}

	public String doAddProduct(){
		Product product = new Product(this.name,this.description,this.price);
		product.setTechnicalInfos(this.getTechnicalInfosInString());
		product.setCost(cost);
		product.setVat(this.vat);
		product.setCategory(categories.get(category-1));
		product.setComposition(compositions.get(composition-1));
		customerGenders =new ArrayList<CustomerGender>();
		product.setCustomerGender(genders.get(gender-1));

		product=this.productService.saveInBase(product);
		
		sportProducts=new ArrayList<SportProduct>();
		for(int s : listOfSports) {
			for(Sport sp : sports) {
				if(sp.getId().equals(s)){
					SportProduct newSp = sportProductService.saveInBase(new SportProduct(sp, product));
					sportProducts.add(newSp);
				}
			}
		}
		
		product.setSportProducts(sportProducts);
		Type type=inventoryService.saveInBase(new Type(this.size,this.color));
		Inventory inventory = inventoryService.saveInBase(new Inventory(this.stock,this.minStock,type));

		inventories=new ArrayList<Inventory>();
		inventories.add(inventory);
		product.setInventories(inventories);

		productService.saveInBase(product);

		message = "Votre article a bien été enregistré";
		return "createProduct.xhtml?faces-redirect=true";

	}
	
	public void doDemo() {
		setName("Chaussure MH 500");
		setDescription("Chaussure Mountain Hiking pour Homme. Confortable, imperméable et adhérente pour vous accompagner dans toutes vos balades montagneuses.");
		setPrice(60.0);
		setCost(30.0);
		setVat(20.0);
		setStock(256);
		setMinStock(35);
		setCategory(5);
		setColor("Noir");
		setSize("44");
		setCompositionQuantity(100.0);
		setComposition(8);
	}
}