package fr.afcepf.al34.project1.spring.springboutique.business;

import java.util.List;

import fr.afcepf.al34.project1.spring.springboutique.entity.Client;
import fr.afcepf.al34.project1.spring.springboutique.entity.Credentials;

public interface ClientService {

	Client searchById(Integer id);
	Client saveInBase(Client client);
	Client findWithCredentials(Credentials credentials);
}
