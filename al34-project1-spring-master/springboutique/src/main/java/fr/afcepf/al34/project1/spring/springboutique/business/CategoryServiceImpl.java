package fr.afcepf.al34.project1.spring.springboutique.business;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import fr.afcepf.al34.project1.spring.springboutique.dao.CategoryDao;
import fr.afcepf.al34.project1.spring.springboutique.dao.CompositionDao;
import fr.afcepf.al34.project1.spring.springboutique.dao.CustomerAgeDao;
import fr.afcepf.al34.project1.spring.springboutique.dao.SportDao;
import fr.afcepf.al34.project1.spring.springboutique.entity.Category;
import fr.afcepf.al34.project1.spring.springboutique.entity.Composition;
import fr.afcepf.al34.project1.spring.springboutique.entity.CustomerAge;
import fr.afcepf.al34.project1.spring.springboutique.entity.Sport;

@Service
@Transactional
public class CategoryServiceImpl implements CategoryService {

	@Autowired
	CustomerAgeDao customerAgeDao;
	@Autowired
	SportDao sportDao;
	@Autowired
	CategoryDao categoryDao;
	@Autowired
	CompositionDao compositionDao;
	@Override
	public CustomerAge saveInBase(CustomerAge ca) {
		return customerAgeDao.save(ca);
	}

	@Override
	public Sport saveInBase(Sport s) {
		return sportDao.save(s);
	}

	@Override
	public Category saveInBase(Category c) {
		return categoryDao.save(c);
	}

	@Override
	public Composition saveInBase(Composition c) {
		return compositionDao.save(c);
	}

	@Override
	public List<Sport> getAllSports() {
		return sportDao.findAll();
	}

	@Override
	public List<Category> getAllCategories() {
		return categoryDao.findAll();
	}

	@Override
	public List<Composition> getAllCompositions() {
		return compositionDao.findAll();
	}

}
