package fr.afcepf.al34.project1.spring.springboutique.business;

import java.util.List;

import fr.afcepf.al34.project1.spring.springboutique.entity.Product;
import fr.afcepf.al34.project1.spring.springboutique.entity.Promotion;

public interface PromotionService {

	List<Promotion> getAllPromotions();
	
	List<Product> findWithPromotion();
	
	Promotion saveInBase(Promotion promo);

}
