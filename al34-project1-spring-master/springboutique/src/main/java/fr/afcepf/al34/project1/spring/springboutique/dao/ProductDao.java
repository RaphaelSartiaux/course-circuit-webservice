package fr.afcepf.al34.project1.spring.springboutique.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import fr.afcepf.al34.project1.spring.springboutique.entity.Category;
import fr.afcepf.al34.project1.spring.springboutique.entity.CustomerGender;
import fr.afcepf.al34.project1.spring.springboutique.entity.Product;
import fr.afcepf.al34.project1.spring.springboutique.entity.Promotion;
import fr.afcepf.al34.project1.spring.springboutique.entity.Sport;

public interface ProductDao extends CrudRepository<Product, Integer> {

	List<Product> findByPromotionNotNull();
//	List <Product> findByCustomerGender(List<CustomerGender> customerGenders);
	List <Product> findByCustomerGenderName(String customerGenderName);
	List <Product> findByCategoryId(Integer categoryId);
	List<Product> findByCustomerGenderNameAndCategoryName(String customerGenderName, String categoryName);
	List<Product> findByCustomerGenderIdAndCategoryId(Integer customerGenderId, Integer categoryId);
	//List<Product> findByCustomerGenderIdAndSportsId(Integer customerGenderId, Integer sportId);
	List <Product> findByCustomerGenderId(Integer customerGenderId);
	
	

}


