package fr.afcepf.al34.project1.spring.springboutique.entity;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter @NoArgsConstructor
@Entity
@Table(name = "grade")
public class Grade implements Serializable{
    
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

    private String name;

    @Column(name = "start_date")
	//@Temporal(TemporalType.DATE)
    private LocalDate startDate;
    
    @ManyToOne
    @JoinColumn(referencedColumnName = "id")
    private Employee employee;

    @Column(name = "end_date")
	//@Temporal(TemporalType.DATE)
    private LocalDate endDate;

}
