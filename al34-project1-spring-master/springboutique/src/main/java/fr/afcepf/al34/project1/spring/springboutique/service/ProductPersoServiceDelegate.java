package fr.afcepf.al34.project1.spring.springboutique.service;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import fr.afcepf.al34.project1.spring.springboutique.dto.DeviseDto;
import fr.afcepf.al34.project1.spring.springboutique.dto.ProductPersoDto;

@Service
public class ProductPersoServiceDelegate implements IProductPersoService{

	
	private RestTemplate restTemplate = new RestTemplate();
	
	@Value("${devise-api.base-url}") //pour récupérer 
	private String baseUrl;

	@Override
	public ProductPersoDto getProductPersoByID(Integer Id, Integer IdClient) {
		String url = baseUrl + "/" +Id+","+IdClient;
		return restTemplate.getForObject(url,ProductPersoDto.class);
	}

	@Override
	public List<ProductPersoDto> getAllProducPersoByIdClient(Integer IdClient) {
		ProductPersoDto[] tabProductPerso = null;
		String url= baseUrl;
		tabProductPerso = restTemplate.getForObject(url, ProductPersoDto[].class, IdClient);
		return Arrays.asList(tabProductPerso);
	}

	@Override
	public List<ProductPersoDto> getAllProducPerso() {
		ProductPersoDto[] tabProductPerso = null;
		String url= baseUrl;
		tabProductPerso = restTemplate.getForObject(url, ProductPersoDto[].class);
		return Arrays.asList(tabProductPerso);
	}

	@Override
	public ProductPersoDto postProductPerso(ProductPersoDto productPerso) {
		String url = baseUrl;
		return restTemplate.postForObject(url,productPerso,ProductPersoDto.class);
	}

	@Override
	public void DeleteProductPerso(ProductPersoDto productPerso) {
		String url = baseUrl;
	restTemplate.delete(url, productPerso);
	}

	
	
}
