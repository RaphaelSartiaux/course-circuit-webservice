package fr.afcepf.al34.project1.spring.springboutique.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import fr.afcepf.al34.project1.spring.springboutique.entity.Credentials;
import fr.afcepf.al34.project1.spring.springboutique.entity.Employee;

public interface EmployeeDao extends CrudRepository<Employee, Integer> {

	List<Employee> findByCredentials(Credentials credentials);
}
