package fr.afcepf.al34.project1.spring.springboutique.entity;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter @NoArgsConstructor
@Entity
@Table(name = "promotion")
public class Promotion implements Serializable{
    
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

    private String name;
    
    private int amount;

    @Column(name = "start_date")
	//@Temporal(TemporalType.TIME)
    private LocalDateTime startDate;
    
    @Column(name = "end_date")
	//@Temporal(TemporalType.TIME)
    private LocalDateTime endDate;
    
    private Date startDateInit;
    
    private Date endDateInit;

    //@Column(name="type_promotion")
    @ManyToOne
    @JoinColumn(referencedColumnName = "id")
    private TypePromotion typePromotion;

    @OneToMany  (mappedBy = "promotion")
    private List<Product> products = new ArrayList<Product> ();
    
//    @ManyToMany  (mappedBy = "promotions")
//    private List<Product> products = new ArrayList<Product> ();
    
    @OneToOne
    private Photo photo;

	public Promotion(String name, int amount, LocalDateTime startDate, List<Product> products) {
		super();
		this.name = name;
		this.amount = amount;
		this.startDate = startDate;
		this.products = products;
	}

	public Promotion(String name, int amount, LocalDateTime startDate, LocalDateTime endDate) {
		super();
		this.name = name;
		this.amount = amount;
		this.startDate = startDate;
		this.endDate = endDate;
	}

	public Promotion(String name, int amount) {
		super();
		this.name = name;
		this.amount = amount;
	}
	
	public Promotion(int amount, LocalDateTime startDate) {
		super();
		this.amount = amount;
		this.startDate = startDate;
	}

}
