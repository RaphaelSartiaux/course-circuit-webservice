package fr.afcepf.al34.project1.spring.springboutique.dao;

import org.springframework.data.repository.CrudRepository;

import fr.afcepf.al34.project1.spring.springboutique.entity.BankCard;

public interface BankCardDao extends CrudRepository<BankCard, Integer> {

}
