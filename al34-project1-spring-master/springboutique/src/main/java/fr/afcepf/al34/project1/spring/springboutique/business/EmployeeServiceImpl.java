package fr.afcepf.al34.project1.spring.springboutique.business;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import fr.afcepf.al34.project1.spring.springboutique.dao.EmployeeDao;
import fr.afcepf.al34.project1.spring.springboutique.entity.Employee;
import fr.afcepf.al34.project1.spring.springboutique.entity.Credentials;

@Service
@Transactional
public class EmployeeServiceImpl implements EmployeeService {

	@Autowired
	EmployeeDao dao;
	
	@Override
	public Employee searchById(Integer id) {
		return dao.findById(id).orElse(null);
	}

	@Override
	public Employee saveInBase(Employee employee) {
		return dao.save(employee);
	}

	@Override
	public List<Employee> findWithCredentials(Credentials credentials) {
		return dao.findByCredentials(credentials);
	}
	
	

}
