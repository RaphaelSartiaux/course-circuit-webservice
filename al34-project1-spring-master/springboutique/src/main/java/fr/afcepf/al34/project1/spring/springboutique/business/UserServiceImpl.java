package fr.afcepf.al34.project1.spring.springboutique.business;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import fr.afcepf.al34.project1.spring.springboutique.dao.GenderDao;
import fr.afcepf.al34.project1.spring.springboutique.dao.UserDao;
import fr.afcepf.al34.project1.spring.springboutique.entity.Gender;
import fr.afcepf.al34.project1.spring.springboutique.entity.User;

@Service
@Transactional
public class UserServiceImpl implements UserService {

	@Autowired
	UserDao userDao;
	
	@Autowired
	GenderDao genderDao;
	
	@Override
	public User saveInBase(User user) {
		return userDao.save(user);
	}

	@Override
	public Gender saveInBase(Gender gender) {
		return genderDao.save(gender);
	}
	
	
	@Override
	public List<Gender> getAllGenders() {
		return (List<Gender>) genderDao.findAll();
	}

}
