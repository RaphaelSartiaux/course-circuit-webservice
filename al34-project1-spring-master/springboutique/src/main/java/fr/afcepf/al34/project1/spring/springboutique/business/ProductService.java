package fr.afcepf.al34.project1.spring.springboutique.business;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import fr.afcepf.al34.project1.spring.springboutique.entity.Photo;
import fr.afcepf.al34.project1.spring.springboutique.entity.Product;
import fr.afcepf.al34.project1.spring.springboutique.entity.Sport;
import fr.afcepf.al34.project1.spring.springboutique.entity.SportProduct;
import fr.afcepf.al34.project1.spring.springboutique.entity.Category;
import fr.afcepf.al34.project1.spring.springboutique.entity.CustomerGender;
import fr.afcepf.al34.project1.spring.springboutique.entity.Inventory;

public interface ProductService {

	Product saveInBase(Product p);
	List<Product> getAllProducts();
	Photo saveInBase(Photo photo);
	
	List<Inventory> getAllInventories();
	//	List <Product> findWithCustomerGender(List<CustomerGender> customerGender);
	List <Product> findWithCustomerGenderName(String customerGenderName);
	List <Product> findWithCategory(Integer categoryId);
	List <Product> findWithGender(Integer genderId);
	List<Product> findWithCustomerGenderNameAndCategoryName(String customerGenderName, String categoryName);
	List<Product> findWithCustomerGenderAndCategory(Integer customerGenderId,Integer categoryId);
	List<Product> findWithCustomerGenderAndSport(Integer customerGenderId,Integer sportId);
}
