package fr.afcepf.al34.project1.spring.springboutique;

import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import fr.afcepf.al34.project1.spring.springboutique.business.ClientService;
import fr.afcepf.al34.project1.spring.springboutique.business.CredentialsService;
import fr.afcepf.al34.project1.spring.springboutique.business.UserService;
import fr.afcepf.al34.project1.spring.springboutique.dao.CredentialsDao;
import fr.afcepf.al34.project1.spring.springboutique.entity.Client;
import fr.afcepf.al34.project1.spring.springboutique.entity.Credentials;
import fr.afcepf.al34.project1.spring.springboutique.entity.Gender;

@SpringBootTest
public class ClientServiceTest {

	@Autowired
	private CredentialsService credentialsService;
	
	@Autowired
	private ClientService clientService;
	
	@Autowired
	UserService userService;
	
	@Autowired
	CredentialsDao credentialsDao;
	
	@Test
	public void testCrudClient() {
		Gender monsieur = userService.saveInBase(new Gender("monsieur"));
		Credentials credentials = new Credentials(null, "client1", "haché", "salé");
		credentialsService.saveInBase(credentials);
		Client client = new Client("Name", "Firstname", "al34.afcepf.com", "0630751896", credentials, monsieur);
		clientService.saveInBase(client);
		Assertions.assertTrue(credentials.getId() > 0);
		Assertions.assertTrue(client.getId() > 0);
	}
	
	@Test
	public void testFindCredentials() {
		Credentials credentials = credentialsDao.findByLogin("client1");
		Assertions.assertTrue(credentials.getClass() != null);
	}
}
