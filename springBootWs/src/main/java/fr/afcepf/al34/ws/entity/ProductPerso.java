package fr.afcepf.al34.ws.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter @Setter @NoArgsConstructor @ToString()
public class ProductPerso {

	@Id
	private Integer id;
	
	@Column(name="idClient")
	private Integer idClient;

	private String name;

	private String description;

	private double price;
	
	private String commentaire;
	
	@ManyToOne
	@JoinColumn(referencedColumnName = "id")
	private Category category;
	
	private boolean vendu;

	public ProductPerso(Integer id, Integer idClient, String name, String description, double price, String commentaire,
			Category category, boolean vendu) {
		super();
		this.id = id;
		this.idClient = idClient;
		this.name = name;
		this.description = description;
		this.price = price;
		this.commentaire = commentaire;
		this.category = category;
		this.vendu = vendu;
	}
	
	
}
