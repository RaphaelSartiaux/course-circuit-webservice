package fr.afcepf.al34.ws.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import fr.afcepf.al34.ws.dao.ProductPersoDao;
import fr.afcepf.al34.ws.entity.ProductPerso;
import fr.afcepf.al34.ws.exception.MyEntityNotFoundException;

@Service
@Transactional
public class ProductPersoServiceImpl implements ProductPersoService {

	@Autowired
	private ProductPersoDao productPersoDao;
	
	@Override
	public ProductPerso saveProductPerso(ProductPerso plop) {
		return productPersoDao.save(plop);
	}

	@Override
	public void deleteProductPerso( Id) throws MyEntityNotFoundException {
		try {
			productPersoDao.deleteById(Id);
		} catch (Exception e) {
			//e.printStackTrace();
			//logger.error("...." , e);
			throw new MyEntityNotFoundException("echec suppression Devise avec code="+code,e);
		}
	}

	@Override
	public List<ProductPerso> findAll() {
		return (List<ProductPerso>) productPersoDao.findAll();
	}

	@Override
	public List<ProductPerso> findAllbyIdClient(Integer idClient) {
		return productPersoDao.findByIdClient(idClient);
	}

}
